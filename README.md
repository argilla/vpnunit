# VPNUnit
Manages our VPN. Lot of details can be found on [this page](https://golem.linux.it/wiki/VPN_del_GOLEM) in Italian.

VPNUnit exposes a REST API interface, and endpoints can be used to administer users and their gateways.

* user: an user corresponds to a member of GOLEM, and is identified by her nickname. (this could enable future integrations with [argento](https://git.golem.linux.it/golem/gestionale))
* gateway: a gateway corresponds to an user device, which can act as a leaf end (eg. her smartphone) or a network router (eg. her *scatolotto*).

An user can have up to 16 gateways. Each gateway is reserved up to 16 /64 networks.

Addresses are given in the following form:

```2001:470:c844:uugn:xxxx:xxxx:xxxx:xxxx```

where

* `2001:470:c844::/48`: GOLEM prefix, courtesy of [Hurrican Electric](https://tunnelbroker.he.net)
* `uu`: user id, from 00 to ff. 00 and 01 are reserved, 02 is for the [Officina Informatica](https://golem.linux.it/wiki/Officina_Informatica).
* `g`: gateway of the user, from 0 to f
* `n`: network *n* of gateway *g* of user *uu*

Using the REST API, the system administrator can manage the `uug` part of the issued addresses:

* create new users
* create and delete (revoke cert) gateways
* obtain the OpenVPN configuration file for the gateways

Since the whole `2001:470:c844:uug0::/60` network will be forwared to/from the user gateway, then she has to manage the *n* part of the address (if she wants), for example using forwarding again and DHCPv6 or SLAAC with radv.

## REST API endpoints
| endpoint              | method | body                                                   | description                                                          |
| --------------------- | :----: | ------------------------------------------------------ | -------------------------------------------------------------------- |
| /users                |  GET   |                                                        | get list of users                                                    |
| /users                |  POST  | ```{ "name": "johndoe" }```                            | create new user                                                      |
| /gateways             |  GET   |                                                        | get list of gateways                                                 |
| /gateways             |  POST  | ```{ "name": "fqdn.domain.tld", "user": "johndoe" }``` | create new gateway for user                                          |
| /gateway/$fqdn        |  GET   |                                                        | get info about gateway                                               |
| /gateway/$fqdn        | DELETE |                                                        | delete gateway                                                       |
| /gateway/$fqdn/config |  GET   |                                                        | get client config file for gateway                                   |
| /gateway/$fqdn/renew  |  POST  |                                                        | generate new certificate for gateway (min 30 days before expiration) |

## Technical details
VPNUnit runs inside a Docker container, and stores its data in the `/data` subvolume (which is usually mounted on `/srv/vpnunit` on the physical machine).

/data contains:

* `database.sqlite3`: users and gateways info
* `pki/`: Public Key Infrastructure for [easy-rsa](https://github.com/OpenVPN/easy-rsa)
* `ovpn/`: server config file, and static clients addresses
* `ip/`: static routes

Example:

1. system administrator creates a new user, database.sqlite3 is updated
2. system administrator creates a new gateway for an user:
    * database.sqlite3 is updated
    * new certificate is issued using easyrsa pki
    * new static client address is registered in `ovpn/clients` in the mentioned form
    * new static route is added in `ip/routes`

Note: the OpenVPN process is actually run on the physical machine!
It uses:

* the server configuration file in `ovpn/`
* the static clients configurations in `ovpn/clients`

Moreover, a cronjob should periodically:

* check for routes in `ip/routes` and update them accordingly on the physical machine.

A possible cronjob could be:
```
while [ true ]; do

		while read line; do
			COMMAND="ip -6 route add $line"
			eval $COMMAND
		done < /srv/vpnunit/ip/routes

	sleep 60
done
```
# Develop
`Dockerfile.develop` contains instructions to build a container for assessing this project's quality, and `script/qa` is automatically run inside that container by the CI system.
Give a look at it.
